var express = require("express"),
    app = express(),
    router = express.Router(),
    async = require("async"),
    http = require("http"),
    pkgcloud = require("pkgcloud");

router.get("/", function(req, res) {
    var client = pkgcloud.providers.rackspace.compute.createClient({
        username: "lifeblue",
        apiKey: "f858223336380452ccfda1a83e4029fc",
        service: "compute",
        region: "DFW"
    });

    client.getServers(function(err, servers) {
        if (err) throw err;

        var response = [];
        async.each(servers, function(server, callback) {
            var serverAddresses = ["23.253.68.223", "23.253.244.30", "23.253.69.240", "23.253.124.101", "198.61.212.20"];

            if (serverAddresses.indexOf(server.openstack.accessIPv4) != -1) {
                var host = "http://" + server.openstack.accessIPv4 + ":4000/api/v1",
                    details = {},
                    body = "",
                    sites = "";

                http.get(host, function(r) {
                    r.setEncoding("utf8");
                    r.on("data", function(chunk) {
                        body += chunk;
                    });
                    r.on("end", function() {
                        sites = JSON.parse(body);
                        sites.sort(function(a,b) { return (a.servername > b.servername) ? 1 : ((b.servername > a.servername) ? -1 : 0); } );

                        details["name"] = server.name;
                        details["status"] = server.status;
                        details["ip_address"] = server.openstack.accessIPv4;
                        details["sites"] = sites;
                        response.push(details);

                        callback();
                    });
                }).on("error", function(e) {
                    console.log("Got error: " + e.message);
                });
            } else {
                callback();
            }
        }, function(err){
            if (err) throw err;
            response.sort(function(a,b) { return (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0); } );

            res.render("index.html", {
                servers: response
            });
        });
    });
});

module.exports = router;