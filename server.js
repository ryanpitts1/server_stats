// Setup server variables
var express = require("express"),
    app = express(),
    router = express.Router(),
    routes = require("./routes/main");

app.use(express.static(__dirname + "/public"));
app.set('views',__dirname + '/views');
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

router.use(function(req, res, next) {
    console.log("Getting server stats...");
    next();
});

// Setup router and define ruotes
app.use(routes);


// Start server
var server = app.listen(4040, function() {
    console.log("Server started!");
});